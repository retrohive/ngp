#!/usr/bin/env bash
# This script takes a directory of nointro roms to update our repository.

CURDIR=$(pwd)

systemid="25"
systemname="neogeopocket"
ext="ngp"

set -e
shopt -s nullglob

update_game()
{
  NEWROM=${1}
  OLDROM=${2}
  PACKAGEDIR=$(dirname ${OLDROM})

  #echo PACKAGEDIR=${PACKAGEDIR}
  NEWSHA1=$(sha1sum ${NEWROM} | awk '{print $1'})
  OLDSHA1=$(sha1sum ${OLDROM} | awk '{print $1'})

  #echo "${NEWSHA1} - ${OLDSHA1}"

  if [ "x${NEWSHA1}" == "x${OLDSHA1}" ]; then
    echo "✅ ${NEWROM}"
  else
    echo "🔄 ${NEWROM}"
    cp ${NEWROM} ${PACKAGEDIR}/
    cd ${PACKAGEDIR}
    updpkgsums
    cd ${CURDIR}
  fi
}

add_game()
{
  file=${1}
  romname=${file%.*}
  mkdir -p Games/"${romname}"
  cp ${TEMPDIR}/${file} Games/"${romname}"
  cd Games/"${romname}"
  PKGFILE=(*.xz)
  if (${PKGFILE[@]}); then
    echo 🌀 rompom -r ${file} -s ${systemid} -n ${romname}
    rompom -r ${file} -s ${systemid} -n ${romname}
    makepkg -cf --nodeps --skipchecksums
  fi
  cd ${CURDIR}
}

if [ $# -ne 1 ]; then
  echo Wrong number of args
  exit 1
fi

IFS=$'\n'

TEMPDIR="${1}"

for file in $(ls ${1}); do
  REALFILE=${file}

  # Checking if file is zipped
  if [ "x${file##*.}" == "xzip" ]; then
    unzip -o "${1}/${file}" >/dev/null 2>&1
    REALFILE="${file%.*}.${ext}"
  fi

  # Skipping Virtual Console roms
  if [[ ${file} =~ .*'Virtual Console'.* ]]; then
    continue
  fi

  if [[ ${file} =~ .*'Enhancement Chip'.* ]]; then
    continue
  fi

  #echo ${REALFILE}

  EXISTING=$(find ./Games/ -name "${REALFILE}")
  if [ "x${EXISTING}" == "x" ]; then
    echo "❌ ROM ${REALFILE} does not exist yet!"
    add_game "${REALFILE}"
  else
    update_game "${REALFILE}" "${EXISTING}"
  fi

  rm "${1}/${REALFILE}"
done

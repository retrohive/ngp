# PC-Engine

based on https://archive.org/details/no-intro_romsets (SNK - Neo Geo Pocket (20220704-094813))

ROM not added :
```sh
❯ tree -hs --dirsfirst
[4.0K]  .
├── [ 64K]  [BIOS] SNK Neo Geo Pocket (Japan) (En) (Beta).bin
└── [ 64K]  [BIOS] SNK Neo Geo Pocket (Japan, Europe) (En).bin

1 directory, 2 files
```
